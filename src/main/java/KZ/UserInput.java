package KZ;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {
        Scanner rd = new Scanner(System.in);
        int inputNumber;
        List<Integer> choosedNumbers = new ArrayList<>();
        System.out.println("Rozpocznij wprowadzanie liczb (0 to zakończenie)");
        inputNumber = rd.nextInt();
        choosedNumbers.add(inputNumber);
        while (inputNumber != 0) {
            System.out.println("Wprowadź kolejną liczbę.");
            inputNumber = rd.nextInt();
            if (inputNumber != 0) {
                choosedNumbers.add(inputNumber);
            } else break;
        }
        System.out.println("wprowadzone liczby to: " + choosedNumbers);
        List<Integer> meanList = new ArrayList<>();
        List<Integer> notMeanList = new ArrayList<>();
        int checkMean;
//        for (int number : choosedNumbers) {
//            checkMean = choosedNumbers.get(number);
//            if (checkMean == 0) {
//                meanList.add(number);
//            }
//            notMeanList.add(number);
//        }
        for (int i = 0; i < choosedNumbers.size(); i++) {
            checkMean = choosedNumbers.get(i);
            if (checkMean % 2 == 0) {
                meanList.add(choosedNumbers.get(i));
            } else if (checkMean % 2 != 0){
                notMeanList.add(choosedNumbers.get(i));
            }
        }
        System.out.println("Liczby parzyste to: " + meanList);
        System.out.println("Liczby nieparzyste to: " + notMeanList);
    }
}
