package KZ;

public class EvenCheck {
    private int checkThisNumber;

    public EvenCheck (int checkThisNumber) {
        this.checkThisNumber = checkThisNumber;
    }

    public void isEven() {
        int evenCheck = checkThisNumber %2;
        if (evenCheck == 0) {
            System.out.println("Podanali liczba jest parzysta.");
        } else {
            System.out.println("Podana liczba nie jest parzysta.");
        }
    }


}