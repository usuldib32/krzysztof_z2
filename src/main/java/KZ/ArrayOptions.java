package KZ;

import java.util.Random;

public class ArrayOptions {
    private int arraySize = 10;
    static int minValue = 100;
    static int minIndex = 0;
    static int maxValue = 0;
    static int maxIndex = 0;
    static int mean = 0;

    public ArrayOptions() {
    }

    public int[] makeArray() {
        Random generator = new Random();
        int randomValue;
        int[] arrayOfInt = new int[arraySize];
        for (int i = 0; i < arraySize; i++) {
            randomValue = generator.nextInt(101);
            arrayOfInt[i] = randomValue;
        }
        return arrayOfInt;
    }

    public static void findMinMax(int[] array) {
        int sum=0;
        for (int i = 0; i < array.length; i++) {
            sum = sum + array[i];
            if (array[i] <= minValue) {
                minValue = array[i];
                minIndex = i;
            } else if (array[i] >= maxValue) {
                maxValue = array[i];
                maxIndex = i;
            }
        }
        mean = sum/array.length;
        System.out.printf("Minimalna wartość tablicy wynosi %d dla indeksu %d\n", minValue, minIndex);
        System.out.printf("Maksymalna wartość tablicy wynosi %d dla indeksu %d\n", maxValue, maxIndex);
        System.out.printf("Srednia wartości tablicy wynosi: %d",mean);
    }

    public static void showArray(int[] array) {
        System.out.println("Tablica wygląda tak: ");
        for (int i=0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

//    public static void showMean(int[] array) {

    }


