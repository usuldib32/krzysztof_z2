package KZ;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 */
public class App {
    static int varA = 0;

    public static void main(String[] args) {
        Scanner rd = new Scanner(System.in);
        do {
            System.out.println("Podaj dodatnią liczbę do sprawdzenia parzystośc: ");
            try {
                varA = rd.nextInt();
            } catch (NumberFormatException e) {
                System.err.println("Wprowadź liczbę całkowitą.");
            } catch (InputMismatchException e) {
                System.out.println("Nie podałeś liczby z zakresi integer.");
            }
        } while (varA < 0);
        EvenCheck userNumber = new EvenCheck(varA);
        userNumber.isEven();
//        int evenCheck = varA %2;
//        if (evenCheck == 0) {
//            System.out.println("Podanali liczba jest parzysta.");
//        } else {
//            System.out.println("Podana liczba nie jest parzysta.");
//        }
        }

    }
