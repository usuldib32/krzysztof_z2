package KZ;

import java.util.Scanner;

public class StringInput {
    public static void main(String[] args) {
        System.out.println("Wprowadż słowo: ");
        Scanner rd = new Scanner(System.in);
        String word = rd.nextLine();
        System.out.println("Wprowadziłeś słowo: " + word);
        int halfWord = (word.length() / 2)+1;   //większa połowa :)
        System.out.println("połowa słowa wygląda tak: ");
        for (int i = 0; i < halfWord; i++) {
            System.out.print(word.charAt(i));
        }
        String palindrome ="";
        System.out.println();
        int hlp = word.length()-1;
        for(int i = 0; i < word.length(); i++) {
            palindrome += word.charAt(hlp--);
        }
//        for (int i = (word.length())-1; i == 0; i--) {
//            palindrome += word.charAt(hlp++);
//        }
        char pomA;

//        System.out.println(palindrome);
        if (word.equals(palindrome)) {
            System.out.println("Wprowadzone słowo jest palindromem.");
        } else {
            System.out.println("Wprowadzone słowo nie jest palindromem.");
        }
    }
}
