package KZ;

public class ArrayMain {
    public static void main(String[] args) {
        ArrayOptions createArray = new ArrayOptions();
        int[] createdArray = createArray.makeArray();
        KZ.ArrayOptions.showArray(createdArray);
        KZ.ArrayOptions.findMinMax(createdArray);
    }
}
